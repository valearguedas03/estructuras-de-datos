//Definici�n de Macros
#define LONGITUD_MAXIMA_NOMBRE 50
#define LONGITUD_MAXIMA_CARNET 12

//Definici�n de Estructuras
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;
}nodo_estudiante;

typedef struct lista_estudiantes
{
	nodo_estudiante *ref_inicio;
	int cantidad;
}lista_estudiantes;

//Definici�n de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe par�metros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento:
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe parametros
	Salidas: No retorna nada
	Funcionamiento:
    - Reserva espacio para ref_lista.
    - Permite que el puntero ref_lista acceda a cantidad y a ref_inicio.
    - ref_inicio apunta a NULL.
    -Crea la lista apuntando a nulo
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: Recibe como parametro el puntero "nuevo" de tipo nodo_estuduiante.
	Salidas: No retorna nada.
	Funcionamiento:
    -Verifica si la lista est� nula,si lo est�, llama a inicializar_lista, para que la cree.
    -En caso contrario, hace que ya no apunte a NULL sino que ahora va a apuntr a inicio, e inicio va a ser el nodo con la informaci�n.
    -Luego se acyualiza cantidad, ya que ya et� agregado un nodo a la lista.
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas: Recibe como parametro el puntero "nuevo" de tipo nodo_estudiante
	Salidas: No retorna nada.
	Funcionamiento:
    - Verifica si la lista est� nula,si lo est�, llama a inicializar_lista, para que la cree.
    -Si la lista est� vac�a pone el nuevo nodo, llamando a insertar_inicio ya que est� vac�a no altera nada.
    -Si la lista posee elementos, temporal toma el lugar del ultimo nodo que se encuentre en la lista.
    -Cuando lo encuentra toma el nuevo nodo y lo coloca.
-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: Recibe como parametro un indice de tipo entero.
	Salidas: No retorna nada.
	Funcionamiento:
    -Compruba si la lista est� vacia, si lo est� pone un mensaje de que La lista est� vac�a y se devuelve.
    -Si el indice no coicide con la cantidad de elemento, da un mensaje de no valido.
    -Si el indice es igual a 0 inicio se actualiza por temporal, se resta a la catidad ya que va a ser eliminado, y se librera el campo de temporal y se devuelve.
    -Mediante un ciclo, va recorrriendo la lista, cuando el contador encuentre al indice buscado, temporal va a tomar el lugar del nodo que le sigue, y se reduce el contador y se devuelve.
-----------------------------------------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: Le ingresa un parametro indice de tipo entero.
	Salidas: Retorna un puntero de tipo nodo_estudiante.
	Funcionamiento:
    -Crea un puntero de temporal de tipo nodo_estudiante y le reserva espacio.
    -Si la lista est� vac�a y la catidad es cero, imprime mensaje se lista vac�a y retorna temporal.
    -Si el indice no coicide con la cantidad de elemento, da un mensaje de no valido y retorna temporal.
    -Ahora nodo temporal va a tomar el lugar de inicio y va a recorrer la lista.
-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: Le ingresan dos parametros de tipo entero
	Salidas: No retorna nada.
	Funcionamiento:
    -Si el carnet almacenado es igual al carnet ingresado ser� correcto, si no imprime el mensaje de que es incorrecto.
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
	menu
	Entradas: No recibe par�metros.
	Salidas: No tiene salidas.
	Funcionamiento:
    -Declara opcion, indice y carnet, y un puntero temporal que accede a funciones de inicio,final y busqueda con indice.
    -Imprime el men� de opciones, y lee el input numerico del usuario, con la restricci�n de que solo es un numero, y con switch, maneja las opciones posibles a realizar.
    -
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: No recibe nig�n par�metro.
	Salidas: Un entero.
	Funcionamiento:
    -Despliega el menu de opciones.
    -Define get_user_input(size_t max_size).
    -Define get_user_numerical_input(size_t max_size).
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: Recibe como parametro un max_size de tipo size_t.
	Salidas: Un puntero de tipo char
	Funcionamiento:
    -Declara un puntero buffer de tipo char y le reserva espacio y declara un size_t characters.
    -Si el buffer es nulo, la memoria no pudo ser revervada.
    -characters alamcena lo que el ususario digita.
    -Despues la ultima pocisi�n del arreglo buffer va a ser ser cero y devuelve el buffer.

----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: Recibe como parametro un max_size de tipo size_t.
	Salidas: Un entero
	Funcionamiento:
    Camia el user input que es un str a un entero y lo retorna.
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);
