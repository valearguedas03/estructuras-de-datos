// RAYCHELL ARGUEDAS BOL�VAR 2019027258
// ESTRUCTURAS DE DATOS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>


struct estudiantes{
	int carnet;
	char nombre[20];
	struct estudiantes *siguiente;
}*inicio = NULL, *nodo = NULL;


// declara la funciones
void menu();
void agregar_estudiante_final();
void agregar_estudiante_inicio();
void verificar_carnet();
void eliminar_estudiante();
int listar();
void mostrar_lista();

int main()
{
 int opcion;

      do
      {
		  menu();
		  scanf("%d", &opcion);
		  fflush(stdin);

          switch(opcion)
          {
             case 1:
                   agregar_estudiante_final();
                   break;

             case 2:
                   agregar_estudiante_inicio();
                   break;

             case 3:
                   verificar_carnet();
                   break;

             case 4:
                   eliminar_estudiante();
                   break;

			 case 5:
                    mostrar_lista();
                    break;
             case 6:
                   exit(0);
				   break;



          }

      }while(opcion != 6);

      return EXIT_SUCCESS; //salir automaticamente del programa

} // MAIN

void menu()
{
    printf("Ingrese una opcion valida: \n");
    printf("\t1.Agregar estudiante al final de la lista\n");
    printf("\t2.Agregar estudiante al inicio de la lista\n");
    printf("\t3.Verificar Carnet del estudiante\n");
	printf("\t4.Eliminar estudiante\n");
	printf("\t5.Mostrar lista de estudiantes\n");
  	printf("\t6.Salir\n");
   	printf("Opcion: ");

}

// 1
void agregar_estudiante_final()
{
      fflush(stdin);
      struct estudiantes *aux = NULL;
      nodo = (struct estudiantes*)malloc(sizeof(struct estudiantes)); // reserva el espacio que va a ocupar el struct

      printf("Digite el nombre del estudiante: ");
      scanf("%s", nodo->nombre);
      printf("Digite el carnet del estudiante: ");
      scanf("%d", &nodo->carnet);
      printf("\n");

      if(inicio == NULL)
      {
            inicio=nodo;
            nodo->siguiente = NULL;
      }
      else
      {
            aux = inicio;
            while(aux->siguiente != NULL)
            {
                  aux = aux->siguiente;
            }
            aux->siguiente = nodo;
            nodo->siguiente = NULL;
      }
        return;
}
// 2
void agregar_estudiante_inicio()
{
      fflush(stdin);
      struct estudiantes *aux = NULL;
      nodo = (struct estudiantes*)malloc(sizeof(struct estudiantes)); // reserva el espacio que va a ocupar el struct

      printf("Digite el nombre del estudiante: ");
      scanf("%s", nodo->nombre);    //guarda la cadena hasta que encuentre un espacio
      printf("Digite el carnet del estudiante: ");
      scanf("%d", &nodo->carnet);
      printf("\n");

      if(inicio == NULL)
      {
            inicio=nodo;
            nodo->siguiente = NULL;
      }
      else
      {
            aux = inicio;
            while(aux->siguiente != NULL)
            {
                  aux = aux->siguiente;
            }
            aux->siguiente = nodo->siguiente;

      }
        return;
}
//3
void verificar_carnet()
{
    int indice=0, posicion,carnet_buscado,nuevo_carnet;

    struct estudiantes *aux = NULL;

      for(aux=inicio;aux!=NULL;aux=aux->siguiente)
      {     listar();
            printf("\t %d.%s \n", indice, aux->carnet);
			printf(" �Que posicion desea validar? ");
 		    scanf("%d", &posicion);
		    printf(" �Cual es el carnet del estudiante en la posicion?\n");
  			scanf ("%d",&carnet_buscado);
  			if( indice == posicion && carnet_buscado == aux->carnet){
                printf("El carnet ingresado es correcto\n");
				system ("PAUSE"); }
            else{
			printf("El carnet ingresado no corresponde a la posicion\n");
			system("PAUSE"); }


      }
}



//4
void eliminar_estudiante ()
{
      fflush(stdin);

      struct estudiantes *aux;
      int indice = 0, estudiante_eliminado = 0, maximo = 0;

      printf("Ingrese el numero de estudiante que desea eliminar: \n");
      maximo = listar();
      printf("Eliminar:  ");
      scanf("%d", &estudiante_eliminado);
      int true=1;
      int false=0;

      nodo = inicio;
      while(true)
      {
            indice++;
            if(indice == estudiante_eliminado ) false;
            else nodo=nodo->siguiente;
      }

//eliminar

      if(nodo == inicio)
      {
            inicio = nodo->siguiente;
            free(nodo);
      }
      else
      {
            aux = inicio;
            while(aux->siguiente != nodo)
            {
                  aux = aux->siguiente;
            }
            aux->siguiente = nodo->siguiente;
            free(nodo);
      }
}
int listar()
{
      struct estudiantes *aux = NULL;
      int indice = 0;

      for(aux=inicio;aux!=NULL;aux=aux->siguiente)
      {
            indice++;
            printf("\t %d.%s \n", indice, aux->nombre);
      }

      return indice;
}

//5
void mostrar_lista()
{

      fflush(stdout);
      struct estudiantes *aux = NULL;

      for(aux=inicio;aux!=NULL;aux=aux->siguiente)
      {
            printf(" -Nombre del estudiante: %s\n",aux->nombre);
            printf(" -Carnet de estudiante: %d\n\n", aux->carnet);

      }
	}
// Para realizar el c�digo obtuve ayuda de videos explicativos y el blog con informaci�n sobre el leguaje C.
// Al mismo tiempo obtuve ayuda de terceros para corregir ciertos errores del c�digo.
