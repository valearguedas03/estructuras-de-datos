#include <stdlib.h>
#include <stdio.h>

struct Nodo{ 
    int entero;
    struct Nodo *izquierda;  
    struct Nodo *derecha;
    struct Nodo *padre;   
};
struct Nodo *crear_Nodo(struct Nodo *padre, int entero);
struct Nodo *crear_Nodo(struct Nodo *padre, int entero){ 
    struct Nodo *nuevo = malloc(sizeof(struct Nodo));
    nuevo->padre = padre; 
    nuevo->entero = entero; 
    return nuevo;}
void agregar_entero(struct Nodo *arbol, int entero);
void agregar_entero(struct Nodo *arbol, int entero){  
    struct Nodo *pivote,*temporal ; 
    int derecha = 0; 
//ESTRUCTURA 
    if(arbol){
        temporal = arbol;
            while(temporal !=NULL){ 
                pivote = temporal; 
            if(entero>temporal->entero){ 
                temporal = temporal->derecha;
                derecha = 1; 
            }else{
                temporal = temporal->izquierda; 
                derecha = 0;}
        }
    }
    temporal = crear_Nodo(pivote,entero);
    if(derecha){
        printf("El numero %i esta a la izquierda de %i\n",entero,pivote->entero); 
        pivote->derecha = temporal; 
    }else{
        printf("El numero %i esta a la derecha %i\n",entero,pivote->entero);
        pivote->izquierda = temporal;}
}
int main(){
    struct Nodo *arbol; 
    arbol = crear_Nodo(NULL,10); // raiz
    agregar_entero(arbol,9); // a la derecha
    agregar_entero(arbol,11);//a la izquierda
    agregar_entero(arbol,6);
    agregar_entero(arbol,5);
    agregar_entero(arbol,2);
    return 0; }