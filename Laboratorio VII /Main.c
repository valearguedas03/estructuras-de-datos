/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Laboratorio 7
    
    Estudiantes: Saul Jimenez 2019027180
                 Raychell Arguedas 

**********************************************************************/

#include <stdio.h>
#include <stdlib.h>

typedef struct persona{
  char *nombre;
  struct persona *sig;
}persona;

typedef struct amigo{
  char *nombre;
  struct amigo *sig;
}amigo;

persona *listaPersona(persona *Lista){
  Lista = NULL;
  return Lista;
}

amigo *listaAmigos(amigo *ListaAmigos){
  ListaAmigos = NULL;
  return ListaAmigos;
}

persona *agregarPersona(persona *Lista, char *nombre){
  persona *nuevaPersona, *aux;
  nuevaPersona = (persona*)malloc(sizeof(persona));
  nuevaPersona->nombre = nombre;
  nuevaPersona->sig = NULL;

  if (Lista == NULL){
    Lista = nuevaPersona;
  }else{
    aux = Lista;
    while(aux->sig != NULL){
      aux = aux->sig;
    }
    aux->sig = nuevaPersona;
  }
  return Lista;
}

amigo *agregarAmigo(amigo *ListaAmigos, char *nombre){
  amigo *nuevoAmigo, *aux;
  nuevoAmigo = (amigo*)malloc(sizeof(amigo));
  nuevoAmigo->nombre = nombre;
  nuevoAmigo->sig = NULL;

  if (ListaAmigos == NULL){
    ListaAmigos = nuevoAmigo;
  }else{
    aux = ListaAmigos;
    while (aux->sig != NULL){
      aux = aux->sig;
    }
    aux->sig = nuevoAmigo;
  }
  return ListaAmigos;
}

int main(){
  persona *Lista = listaPersona(Lista);
  amigo *ListaAmigos = listaAmigos(ListaAmigos);
  Lista = agregarPersona(Lista,"Raychell: ");
  ListaAmigos = agregarAmigo(ListaAmigos, "Andres, Saul, Johan, Te, Maria, Pablo");
  Lista = agregarPersona(Lista, "Maria: ");
  ListaAmigos = agregarAmigo(ListaAmigos, "Steven, Raychell, Kevin C, Te, Randy, Bryan, Johan, Andres, Marco, Pablo, Fernando, Randall");
  Lista = agregarPersona(Lista, "Kevin C: ");
  ListaAmigos = agregarAmigo(ListaAmigos, "Pablo, Steven, Maria, Te, Fernando, Marco, Kevin Z, Luis Diego, Bryan, Joseph, Andres, Johan, Randy");
  Lista = agregarPersona(Lista, "Te: ");
  ListaAmigos = agregarAmigo(ListaAmigos, "Raychell, Maria, Randall, Kevin Z, Kevin C, Randy, Marco, Steven, Johan, Jose Andres, Joseph, Fernando, Alejandro");
  Lista = agregarPersona(Lista, "Randy: ");
  ListaAmigos = agregarAmigo(ListaAmigos, "Maria, Te, Randall, Kevin Z, Kevin C, Randy, Marco, Steven, Johan, Jose Andres, Joseph, Fernando, Alejandro");
  Lista = agregarPersona(Lista, "Bryan: ");
  ListaAmigos = agregarAmigo(ListaAmigos, " Te, Randall, Kevin C, Steven, Johan, Saul, Andres");
  Lista = agregarPersona(Lista, "Alejandro: ");
  ListaAmigos = agregarAmigo(ListaAmigos, "Will, Joseph, Fernando, Randall, Raychell");
  Lista = agregarPersona(Lista, "Johan: ");
  ListaAmigos = agregarAmigo(ListaAmigos, " Te, Randall, Kevin C, Steven, Johan, Andres");
  Lista = agregarPersona(Lista, "Saul: ");
  ListaAmigos = agregarAmigo(ListaAmigos, " Te, Maria,  Randall, Kevin C, Steven, Johan, Saul, Andres, Marco");
  Lista = agregarPersona(Lista, "Andres: ");
  ListaAmigos = agregarAmigo(ListaAmigos, " Te, Steven, Bryan, Raychell,Kevin");
  Lista = agregarPersona(Lista, "Marco: ");
  ListaAmigos = agregarAmigo(ListaAmigos, " Te, Randall, Kevin C, Saul, Maria, Kevin Z");
  Lista = agregarPersona(Lista, "Pablo: ");
  ListaAmigos = agregarAmigo(ListaAmigos, "Maria, Te, Randall, Kevin Z, Kevin C, Randy, Marco, Steven, Johan, Jose Andres, Joseph, Fernando, Alejandro");
  Lista = agregarPersona(Lista, "Pablo: ");
  ListaAmigos = agregarAmigo(ListaAmigos, "Maria, Te, Randall, Kevin Z, Kevin C, Randy, Marco, Steven, Johan, Jose Andres, Joseph, Fernando, Alejandro");
  Lista = agregarPersona(Lista, "Fernando: ");
  ListaAmigos = agregarAmigo(ListaAmigos,"Will, Joseph,Fernando, Randall, Maria, Te, Saul, Johan, Kevin");
  Lista = agregarPersona(Lista, "Fernando: ");
  ListaAmigos = agregarAmigo(ListaAmigos,"Will,Fernando, Randall, Maria, Te, Saul, Johan, Kevin C, Raychell, Pablo, Alejandro, Steven");
  Lista = agregarPersona(Lista, "Kevin Z: ");
  ListaAmigos = agregarAmigo(ListaAmigos,"Randy, Steven, Raychell, Marco, Randall, Saul, Johan");
  Lista = agregarPersona(Lista, "Randall: ");
  ListaAmigos = agregarAmigo(ListaAmigos,"Alejandro, Randy, Pablo, Joseph, Maria, Te, Saul, Johan, Kevin, Marco");


  while (Lista != NULL){
    printf("%s", Lista->nombre), printf("%s\n",ListaAmigos->nombre);
    Lista = Lista->sig;
    ListaAmigos = ListaAmigos->sig;

  }
  return 0;
  }